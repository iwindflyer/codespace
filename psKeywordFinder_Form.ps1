<# 
文件：psKeywordFinder_Form.ps1
用途：用於查找JSP檔案內有<form>關鍵字的功能腳本
建立：2020-06-30
修改：2020-07-07
作者：壽程科、莊幃復
功能：查找、壓縮、發信
#> 

#初始化變數，末碼需有\
$SCAN_FOLDER="D:\antspace70\"
#D:\antspace70\CSR\
$OUTPUT_FOLDER="D:\Powershell\psKeywordFinder_Form\"
#輸出檔案
$OUTPUT_FILE=$OUTPUT_FOLDER+'psKeywordFinder_Form_log.zip'

#輸出檔案路徑與名稱

#FORM
$ALL=$OUTPUT_FOLDER+"ALL.txt"
$ALL_DETAIL=$OUTPUT_FOLDER+"ALL_DETAIL.txt"

#FORM為POST
$POST=$OUTPUT_FOLDER+"POST.txt"
$POST_DETAIL=$OUTPUT_FOLDER+"POST_DETAIL.txt"

#FORM為GET或者沒有
$GET=$OUTPUT_FOLDER+"GET.txt"
$GET_DETAIL=$OUTPUT_FOLDER+"GET_DETAIL.txt"

#FORM為GET或者沒有，且有submit關鍵字
$SUBMIT=$OUTPUT_FOLDER+"SUBMIT.txt"
$SUBMIT_DETAIL=$OUTPUT_FOLDER+"SUBMIT_DETAIL.txt"


#查找樣式
$MATCH_PATTERN_FORM='<form.*>'
#查找檔案類型
$FILE_EXT='.jsp'

# 開始計時
$sw = [Diagnostics.Stopwatch]::StartNew()


#先清除舊檔
if(Test-Path -path $ALL){
  Remove-Item $ALL
}
if(Test-Path -path $ALL_DETAIL){
  Remove-Item $ALL_DETAIL
}
if(Test-Path -path $POST){
  Remove-Item $POST
}
if(Test-Path -path $POST_DETAIL){
  Remove-Item $POST_DETAIL
}
if(Test-Path -path $GET){
  Remove-Item $GET
}
if(Test-Path -path $GET_DETAIL){
  Remove-Item $GET_DETAIL
}
if(Test-Path -path $SUBMIT){
  Remove-Item $SUBMIT
}
if(Test-Path -path $SUBMIT_DETAIL){
  Remove-Item $SUBMIT_DETAIL
}
if(Test-Path -path $OUTPUT_FILE){
  Remove-Item $OUTPUT_FILE
}


Write-Host 掃描下列資料夾：  $SCAN_FOLDER
Write-Host 預計寫入資料夾：  $OUTPUT_FOLDER




#delay幾秒才開始
Start-Sleep -s 1

#顯示form
Get-ChildItem $SCAN_FOLDER -recurse | where {$_.extension -eq $FILE_EXT} | % {
    if((Get-Content $_.FullName) -match $MATCH_PATTERN_FORM) {
      #Write-Host $_.FullName -ForegroundColor  DarkYellow
      #(Get-Content $_.FullName) -match $MATCH_PATTERN_FORM
      #詳細輸出
      $_.FullName >> $ALL_DETAIL
      (Get-Content $_.FullName) -match $MATCH_PATTERN_FORM  >> $ALL_DETAIL
      
      #只輸出LIST
      Write-Output $_.FullName  >> $ALL
    }
}

#顯示form post，依照$ALL當作來源
$MATCH_PATTERN_FORM='<form.*post*'
Get-Content $ALL | ForEach-Object {
    
    if((Get-Content $_) -match $MATCH_PATTERN_FORM) {
      #Write-Host $_ -ForegroundColor  DarkGreen
      #(Get-Content $_) -match $MATCH_PATTERN_FORM
      #詳細輸出
      $_ >> $POST_DETAIL
      (Get-Content $_) -match $MATCH_PATTERN_FORM >> $POST_DETAIL
      #只輸出LIST
      Write-Output $_  >> $POST
    }
}


#比較兩個物件
Compare-Object (Get-Content $ALL) (Get-Content $POST)  | Select -Property InputObject | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1 | Out-File $GET


#顯示GET的DETAIL

$MATCH_PATTERN_FORM='<form.*>'
Get-Content $GET | ForEach-Object {
    #替換double quote
    $_ = $_.Replace("`"","")
    if((Get-Content $_) -match $MATCH_PATTERN_FORM) {
      Write-Host $_ -ForegroundColor  DarkGreen
      (Get-Content $_) -match $MATCH_PATTERN_FORM
      #詳細輸出
      $_ >> $GET_DETAIL
      (Get-Content $_) -match $MATCH_PATTERN_FORM >> $GET_DETAIL
    }
}


#抓取FORM有submit關鍵字
$MATCH_PATTERN_FORM_SUBMIT='submit'
$NOTMATCH_PATTERN_FORM_SUBMIT='//submit'
Get-Content $GET | ForEach-Object {
    #替換double quote
    $_ = $_.Replace("`"","")
    #有FORM為空、GET的情況下
    if((Get-Content $_) -match $MATCH_PATTERN_FORM) {
      #有submit但沒有//submit的案例
        if((Get-Content $_) -match $MATCH_PATTERN_FORM_SUBMIT -notmatch $NOTMATCH_PATTERN_FORM_SUBMIT) {
          #詳細輸出
          Write-Host $_ -ForegroundColor  DarkGreen
          $_ >> $SUBMIT_DETAIL
          (Get-Content $_) -match $MATCH_PATTERN_FORM
          (Get-Content $_) -match $MATCH_PATTERN_FORM >> $SUBMIT_DETAIL
          (Get-Content $_) -match $MATCH_PATTERN_FORM_SUBMIT
          (Get-Content $_) -match $MATCH_PATTERN_FORM_SUBMIT >> $SUBMIT_DETAIL
          #只輸出LIST
          Write-Output $_  >> $SUBMIT
        }
    }
}


# 停止計時
$sw.Stop()
Write-Host "#######################處理完畢#######################"
# 輸出測量時間
$TotalMinutes = $sw.Elapsed.TotalMinutes

Write-Host 處理完畢，總計花費了： $TotalMinutes 分 


#LOG檔案壓縮
Compress-Archive -Path $OUTPUT_FOLDER -DestinationPath $OUTPUT_FILE

#[void][System.Console]::ReadKey($FALSE)